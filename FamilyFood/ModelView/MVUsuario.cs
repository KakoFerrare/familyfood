﻿using FamilyFood.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFood.ModelView
{
     public class MVUsuario
    {
        public Usuario usuario { get; set; }
        public MVUsuario()
        {
            usuario = new Usuario();
        }
    }
}
