﻿using FamilyFood.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFood.DAO
{
    public class DaoUsuario
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "usuarios";
        private MySqlConnection conn = new MySqlConnection();
        private string connection = Properties.Settings.Default.connectionString;

        internal int Cadastrar(Usuario usuario)
        {
            strQuery = new StringBuilder();
            strQuery.Append(" insert into " + table + " (nomeCompleto, apelido, email, senha, dataCadastro) VALUES (@nomeCompleto, @apelido, @email, @senha, @dataCadastro)");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@nomeCompleto", usuario.NomeCompleto);
                        cmd.Parameters.AddWithValue("@apelido", usuario.Apelido);
                        cmd.Parameters.AddWithValue("@email", usuario.Email);
                        cmd.Parameters.AddWithValue("@senha", usuario.Senha);
                        cmd.Parameters.AddWithValue("@dataCadastro", DateTime.Now);
                        usuario.IdUsuario = cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    usuario.IdUsuario = 0;
                    usuario.NomeCompleto = "Erro em encontrar o usuário! " + e.Message;
                }
                conn.Close();
            }
            return usuario.IdUsuario;
        }

        public Usuario Login(string login, string senha)
        {
            Usuario usuario = new Usuario();
            strQuery = new StringBuilder();
            strQuery.Append(" Select * from " + table + " WHERE email = @login AND senha = @senha");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@login", login);
                        cmd.Parameters.AddWithValue("@senha", senha);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (PropertyInfo propertyInfo in usuario.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item" && propertyInfo.Name != "ConfirmarSenha")
                                    propertyInfo.SetValue(usuario, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                            }
                            else
                            {
                                usuario.IdUsuario = 0;
                                usuario.NomeCompleto = "Usuário não encontrado, verifique suas credenciais.";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    usuario.IdUsuario = 0;
                    usuario.NomeCompleto = "Erro em encontrar o usuário! " + e.Message;
                }
            }
            return usuario;
        }
    }
}
