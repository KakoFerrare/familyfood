﻿using FamilyFood.DAO;
using FamilyFood.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFood.CN
{
    public class CnUsuario
    {
        private DaoUsuario DAOUsuario = new DaoUsuario();
        public Usuario Login(string login, string senha)
        {
            return DAOUsuario.Login(login, senha);
        }

        internal int Cadastrar(Usuario usuario) => DAOUsuario.Cadastrar(usuario);
    }
}
