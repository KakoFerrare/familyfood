﻿using FamilyFood.Model;
using FamilyFood.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FamilyFood
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Cadastrar(object sender, RoutedEventArgs e)
        {
            Cadastrar cadastrar = new Cadastrar();
            this.Hide();
            cadastrar.ShowDialog();
        }

        private void Logout(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoginApp(object sender, RoutedEventArgs e)
        {
            Usuario usuario = new CN.CnUsuario().Login(LoginUser.Text, PasswordUser.Password);
            if (usuario.IdUsuario > 0)
            {
                if (usuario.Ativo == 1)
                {
                    MainWindow main = new MainWindow();
                    this.Close();
                    main.Show();
                }
                else
                {
                    ErrorMessagemLogin.Text = "Usuário já cadastrado, aguardando aprovação do administrador";
                }
               
            }
            else
            {
                ErrorMessagemLogin.Text = usuario.NomeCompleto;
            }
        }
    }
}
