﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFood.Model
{
    public class Ingrediente
    {
        public int IdIngrediente { get; set; }
        public int IdReceita { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public int IdTipoMedida { get; set; }
    }
}
