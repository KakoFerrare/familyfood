﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFood.Model
{
    public class Role
    {
        public int IdRole { get; set; }
        public string Nome { get; set; }
    }
}
