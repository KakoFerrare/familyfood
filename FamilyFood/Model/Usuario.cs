﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFood.Model
{
    public class Usuario : PropertyValidateModel
    {
        public int IdUsuario { get; set; }
        [Required]
        [Display(Name = "Nome Completo")]
        [MinLength(5, ErrorMessage = "O tamanho mínimo do nome são 3 caracteres.")]
        public string NomeCompleto { get; set; }

        [Required]
        [Display(Name = "Apelido")]
        public string Apelido { get; set; }


        //[DisplayFormat(DataFormatString = "{0:n2}",
        //   ApplyFormatInEditMode = true,
        //   NullDisplayText = "Sem preço")]
        //[Range(3, 500, ErrorMessage = "O preço deverá ser entre 3 e 500.")]
        //[Column(Order = 3)]
        //public decimal Preco { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        //[Column(Order = 4)]
        //[Display(Name = "Vencimento")]
        //public DateTime Dt_Vencto { get; set; }

        //[Display(Name = "Quantidade")]
        //[DisplayFormat(DataFormatString = "{0:n2}",
        //ApplyFormatInEditMode = true,
        //NullDisplayText = "Estoque vazio")]
        //[Range(10, 25, ErrorMessage = "A Qtde deverá ser entre 10 e 25.")]
        //[Column(Order = 2)]
        //public double Qtde { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        public string Senha { get; set; }

        public string ConfirmarSenha { get; set; }

        public SByte Ativo { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime? DataAtivacao { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public int? IdRole { get; set; }
    }
}
