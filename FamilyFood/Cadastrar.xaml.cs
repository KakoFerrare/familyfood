﻿using FamilyFood.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace FamilyFood
{
    /// <summary>
    /// Interaction logic for Cadastrar.xaml
    /// </summary>
    public partial class Cadastrar : Window
    {
        ICollection<System.ComponentModel.DataAnnotations.ValidationResult> results = null;

        Notifier notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomRight,
                offsetX: 5,
                offsetY: 0);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        private Usuario usuario = new Usuario();
        public Cadastrar()
        {

            InitializeComponent();
            this.DataContext = usuario;
        }

        private void CadastrarUsuario(object sender, RoutedEventArgs e)
        {
            if (Validator.TryValidateObject(usuario, new ValidationContext(usuario), results, true))
            {
                if ((Senha.Password != ConfirmarSenha.Password) || (string.IsNullOrEmpty(Senha.Password) && string.IsNullOrEmpty(ConfirmarSenha.Password)))
                {
                    Senha.Password = "";
                    ConfirmarSenha.Password = "";
                    notifier.ShowError("Verifique as senhas...");
                }
                else
                {
                    usuario.Senha = Senha.Password;

                    if(new CN.CnUsuario().Cadastrar(usuario) > 0)
                    {
                        Login login = new Login();
                        this.Close();
                        login.Show();
                    }
                    else
                    {
                        notifier.ShowError("Houve um erro em cadastrar o usuario, tente novamente...");
                    }
                }
            }
        }

        private void Cancelar(object sender, RoutedEventArgs e)
        {
            this.Close();
            Login login = new Login();
            login.Show();
        }
    }
}
